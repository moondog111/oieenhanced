#include "Board.h"

Board::Board(int lc)
{
    limitCase=lc;
    for (int i=0; i<limitCase; i++)
    {
        c=new Case(i);
        theBoard.push_back(c);
        if(i%7==0 && i!=0)
        {
            Bonus *b=new Bonus(4);
            c->addEvent(b);
        }

        if(i%13==0 && i!=0)
        {
            Penalty *p=new Penalty(2);
            c->addEvent(p);
        }
        if(i%9==0 && i!=0)
        {
            Bonus *b=new Bonus();
            c->addEvent(b);
        }
    }
}

Case* Board::getCase(int nb)
{
    return theBoard[nb];
}

Board::~Board()
{
    for (int i=0; i<theBoard.size(); i++)
    {
        delete theBoard[i];
    }
}
