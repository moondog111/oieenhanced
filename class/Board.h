#ifndef BOARD_H
#define BOARD_H

#include "Case.h"
#include <iostream>
#include <vector>
#include "Bonus.h"
#include "Penalty.h"

class Board
{
    public:
        Board(int lc);
        Case* getCase(int nb);
        virtual ~Board();
    protected:

    private:
    Case *c;
    std::vector<Case*>theBoard;
    int limitCase;
    int intervEvent;

};

#endif // BOARD_H
