#ifndef BONUS_H
#define BONUS_H

#include "Event.h"


class Bonus : public Event
{
    public:
        Bonus(int cM);
        Bonus();
        int getNumOfEvent();

        virtual ~Bonus();
    protected:
    private:
        int moveBonus;
};

#endif // BONUS_H
