#ifndef CASE_H
#define CASE_H

#include "Event.h"



class Case
{
    public:
        Case(int nb);
        int getNum();
        bool isThereEvent();
        void addEvent(Event *e);
         Event* getEvent();
    virtual ~Case();

    protected:
    private:
    int number;
    Event *event;
};

#endif // CASE_H
