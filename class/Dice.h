#ifndef DICE_H
#define DICE_H
#include <stdio.h>
#include <stdlib.h>

class Dice
{
    public:
        Dice();
        static int throwDice();
        virtual ~Dice();

    protected:
    private:
};

#endif // DICE_H
