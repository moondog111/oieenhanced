#ifndef EVENT_H
#define EVENT_H
#include <iostream>
#include <string>

using namespace std;

class Event
{
public:
    Event();
    string getType();
    virtual int getNumOfEvent() = 0;
    virtual ~Event();
protected:

    string type;

private:
};

#endif // EVENT_H
