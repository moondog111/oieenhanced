#include "Game.h"

Game::Game()
{
    boardLimit=63;
    board= new Board(boardLimit);

    nbPlayers=0;
    playerNumList=0;
    nbTurn=1;
    nbGoBack=0;
    currentCase=0;
    currentEvent=0;
    nbMove=0;

    playerList.resize(0);
    playerPostion.resize(0);
    playerPenalty.resize(0);

    isEndOfT=false;
    isEndOfG=false;
    haveExceedLimit=false;
}

void Game::nextPlayerTurn()
{
    currentPlayer=playerList[playerNumList];
}

void Game::addPlayer(string nom)
{
    nbPlayers++;
    Player *p=new Player(nbPlayers, nom);
    playerList.push_back(p);
    playerPostion.push_back( board->getCase(0) ) ;
    playerPenalty.push_back(0);
}

bool Game::isEndOfTurn()
{
    return isEndOfT;
}


void Game::move(int num)
{
    haveExceedLimit=false;
    nbMove=num;
    nbCurrentPlayerMove =playerPostion[playerNumList]->getNum();
    if(nbCurrentPlayerMove+nbMove>boardLimit-1)
    {
        nbGoBack =nbCurrentPlayerMove+nbMove-(boardLimit-1);
        playerPostion[playerNumList]=board->getCase((boardLimit-1)-nbGoBack);
        haveExceedLimit=true;

    }
    else if (nbCurrentPlayerMove+nbMove<0)
    {
        playerPostion[playerNumList]=board->getCase(0);
    }
    else if( nbCurrentPlayerMove+nbMove==boardLimit-1)
    {
        playerPostion[playerNumList]=board->getCase(boardLimit-1);
        isEndOfT=true;
        isEndOfG=true;
    }
    else
    {
        playerPostion[playerNumList]=board->getCase(nbCurrentPlayerMove+nbMove);
    }
    currentCase= playerPostion[playerNumList ];
}

int Game::throwDice()

{
    return Dice::throwDice();
}


string Game::getCurrentPlayerName()
{
    return currentPlayer->getName();
}


int Game::getPlayerPosition()
{
    return currentCase->getNum();
}

bool Game::hasHeExceedsLimit()
{
    return haveExceedLimit;
}

int Game::getNbOfPlayers()
{
    return playerList.size();
}

void Game::nextTurn()
{
    isEndOfT=false;
}

bool Game::isEnd()
{
    return isEndOfG;
}

int Game::getNbTurn()
{
    return nbTurn;;
}

bool Game::isThereEvent()
{
    return currentCase->isThereEvent();
}

string Game::getTypeEvent()
{
    currentEvent=currentCase->getEvent();
    typeEvent=currentEvent->getType();
    return typeEvent;
}

void Game::activeEvent()
{

    if(typeEvent=="moveBonus")
    {
        numEvent=currentEvent->getNumOfEvent();
        this->move(numEvent);
    }
    else if(typeEvent=="turnPenalty")
    {
        numEvent=currentEvent->getNumOfEvent();
        playerPenalty[playerNumList]=numEvent;
    }
    else if(typeEvent=="doubleMove")
    {
        this->move(nbMove);
    }

}


void Game::endOfPlayerTurn()
{
    if(numPlayerPenalty != 0)
    {
        numPlayerPenalty--;
        playerPenalty[playerNumList]=numPlayerPenalty;
    }
    playerNumList++;
    if(playerNumList==playerList.size() )
    {
        isEndOfT=true;
        nbTurn++;
        playerNumList=0;
    }

}

int Game::getNumOfEvent()
{
    return numEvent;
}

int Game::checkPenalty()
{
    numPlayerPenalty=playerPenalty[playerNumList];
    return numPlayerPenalty;
}

int Game::getBoardLimit()
{
    return boardLimit-1;

}
Game::~Game()
{
    for(int i=0; i<playerList.size(); i++)
    {
        delete playerList[i];
        delete playerPostion[i];
    }
}
