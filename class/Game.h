#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <vector>
#include <string>

#include "Player.h"
#include "Case.h"
#include "Board.h"
#include "Dice.h"



class Game
{
public:
    Game();
    void addPlayer( string nom);
    void nextPlayerTurn();
    void endOfPlayerTurn();
    int getPlayerPosition();
    int getNbOfPlayers();
    void nextTurn();
    void move(int num);
    bool isEnd();
    string getCurrentPlayerName();
    bool isEndOfTurn();
    bool hasHeExceedsLimit();
    int getNbTurn();
    int throwDice();
    int checkPenalty();
    bool isThereEvent();
    string getTypeEvent();
    int getNumOfEvent();
    void activeEvent();
    int getBoardLimit();

    virtual ~Game();


protected:


private:
    int nbCurrentPlayerMove,nbPlayers,nbTurn,playerNumList,boardLimit,numEvent,numPlayerPenalty,nbGoBack,nbMove;
    bool isEndOfT, isEndOfG,haveExceedLimit;

    Board *board;
    Player *currentPlayer;
    Case *currentCase;
    Event *currentEvent;

    std::vector<Case*> playerPostion;
    std::vector<Player*> playerList;
    std::vector<int> playerPenalty;

    string typeEvent;



};

#endif // GAME_H
