#ifndef PENALTY_H
#define PENALTY_H

#include "Event.h"


class Penalty : public Event
{
    public:
        Penalty(int tS);
        int getNumOfEvent();
        virtual ~Penalty();

    protected:
    private:
        int nbOfTs;
};

#endif // PENALTY_H
