#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>

#include "Case.h"
#include "Board.h"

using namespace std;


class Player
{
public:
    Player(int num,string name);
    string getName();
    int getNum();

    virtual ~Player();
protected:
private:

    Case *c;
    int num;
    string name;
    Case *location;

};

#endif // PLAYER_H
