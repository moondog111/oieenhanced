
#include "main.h"





int main()
{
    srand(time(NULL));
    wantToReplay[0]='o';

    while(strcmp(wantToReplay,"o")==0)
    {
        system("cls");
          cout << "------Bienvenue au jeu de l'oie enhanced edition !------"<< endl;
        cout << toAscii("Une nouvelle partie va d�buter !")<< endl;
        Game *newGame = new Game();

        do
        {
            if(newGame->getNbOfPlayers()<2)
            {
                cout <<"  Vous devez ajouter au moins 2 joueurs pour jouer !" <<endl;
            }
            cout << "  Ajouter un joueur ? o/n" << endl;
            cin >> wantANewPlayer;

            if(strcmp(wantANewPlayer,"o")==0)
            {
                cout << "  Choisissez un nom :"<< endl;
                cin>> playerName;
                newGame->addPlayer(playerName);
                cout <<"  "<<playerName << toAscii(" a �t� ajout� � la partie ") << endl;
            }
        }
        while( (strcmp(wantANewPlayer,"n")!=0)||(newGame->getNbOfPlayers()<2) )  ;
        cout << "  " << endl;
        while(newGame->isEnd()==false)
        {

            cout <<  toAscii("          D�butez le prochain tour ! Appuyez sur une touche !") <<endl;
            getch();
            system("cls");

            newGame->nextTurn();
            cout << "---------------- TOUR " <<newGame->getNbTurn() <<" ---------------- "<< endl;

            while(newGame->isEndOfTurn() == false)
            {

                newGame->nextPlayerTurn();
                if(newGame->checkPenalty()==0)
                {

                    cout << newGame->getCurrentPlayerName()<< toAscii(" c'est � vous de jouer !") << endl;
                    cout <<  toAscii("          Appuyez sur une touche pour lancer les d�s !") ;
                    getch();
                    cout<<"\r                                                           "<<endl;

                    nb2=newGame->throwDice();
                    nb1=newGame->throwDice();

                    cout <<    "  Vous avez fait "<< nb2<< " et "<<nb1 << endl;
                    newGame->move(nb1+nb2);
                    cout <<  toAscii("  Vous �tes � la case n�")<< newGame->getPlayerPosition()  <<" sur "<< newGame->getBoardLimit() << endl;

                    if(newGame->isThereEvent())
                    {
                        cout <<     toAscii("     Vous etes tomb� sur une case evenement ")<< endl;
                        if(newGame->getTypeEvent()=="moveBonus")
                        {
                            newGame->activeEvent();
                            cout <<    "     Vous avancez de "<<newGame->getNumOfEvent() <<" cases"<< endl;
                            cout <<    toAscii("     Vous �tes maintenant � la  case n�") <<newGame->getPlayerPosition() <<" sur "<< newGame->getBoardLimit()<< endl;
                        }
                        else if(newGame->getTypeEvent()=="turnPenalty")
                        {
                            newGame->activeEvent();
                            cout << "     Vous tombez sur un malus, vous devez passer " <<newGame->getNumOfEvent()<<" fois votre tour"<< endl;

                        }
                        else if(newGame->getTypeEvent()=="doubleMove")
                        {
                            newGame->activeEvent();                                                              // il est n�cessaire de mettre cette ligne dans chaque comparaison, si la case du joueur est modifi� alors le getTypeEvent() peut poser probl�me
                            cout <<   toAscii( "     Vous doublez votre jet de d�s !")<< endl;
                            cout <<    toAscii("     Vous �tes maintenant � la case n�") <<newGame->getPlayerPosition() << endl;
                        }

                    }
                    if(newGame->hasHeExceedsLimit())
                    {
                        cout << toAscii("N'oubliez pas qu'il faut tomber pile sur la derni�re case! Sinon vous reculez !") << endl;
                    }
                    cout << "  " << endl;
                }
                else
                {
                    cout << "  "<<newGame->getCurrentPlayerName()<<" vous passez votre tour !" << endl;
                    cout << "  " << endl;
                }
                newGame->endOfPlayerTurn();

            }
        }
        cout << newGame->getCurrentPlayerName()<< toAscii(" vous avez atteint la derni�re case! Vous avez gagn� !!") << endl;
        cout <<" " << endl;
        do
        {
            cout <<" rejouer? o/n" << endl;
            cin >>wantToReplay ;
        }
        while( (strcmp(wantToReplay,"o")!=0) && strcmp(wantToReplay,"n")!=0)  ;
        cout <<" " << endl;
    }
    return 0;
}

string toAscii(char tab[])
{
    char buffer[256];
    CharToOemA(tab, buffer);
    string str(buffer);
    return str;
}


